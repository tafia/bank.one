echo "\nregistering tafia"
curl http://localhost:3000/register/user \
    -H "Content-Type: application/json" \
    --data '{"login": "tafia", "firstname": "Johann", "lastname": "Tuffe", "password": "pass" }' \
    -H "Accept: application/json"

echo "\nregistering tafia again (error)"
curl http://localhost:3000/register/user \
    -H "Content-Type: application/json" \
    --data '{"login": "tafia", "firstname": "Johann", "lastname": "Tuffe", "password": "pass" }' \
    -H "Accept: application/json"

echo "\nlogin"
curl http://localhost:3000/user \
    -H "Content-Type: application/json" \
    --data '{"login": "tafia", "password": "pass" }' \
    -H "Accept: application/json" \
    --verbose

echo "\nget"
curl http://localhost:3000/user -H "Accept: application/json" \
    --cookie token=siwKnJu/eoIqkW6e1Bs9wfU4ZlKCg2DZpQbI/qU0rNGzNVIxt+15M49bfRWgDG9VBr4rD7kLDZtuO+qH7Ty/yNGYPRc9G+hG6NwORPuTbH5pDXcSrhyEQFTs0l7esd8iUB88

