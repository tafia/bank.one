use crate::{errors::Error, State};
use actix_identity::Identity;
use actix_web::{
    dev::Payload,
    error::{Error as ActixError, ErrorUnauthorized},
    web, FromRequest, HttpRequest,
};
use bank::{
    database::{AccountId, UserId},
    user::User,
};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct RegisterUser {
    login: String,
    firstname: String,
    lastname: String,
    password: String,
}

/// A slim user
#[derive(Debug, Serialize, Deserialize)]
pub struct SlimUser {
    pub id: UserId,
    pub firstname: String,
    pub lastname: String,
    pub accounts: Vec<AccountId>,
    pub transaction_count: usize,
}

/// Register a new user
pub fn register(user: web::Json<RegisterUser>, state: web::Data<State>) -> Result<(), Error> {
    let mut db = state.database.write().unwrap();
    let web::Json(RegisterUser {
        login,
        firstname,
        lastname,
        password,
    }) = user;
    db.register_user(login, firstname, lastname, password)?;
    Ok(())
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AuthData {
    pub login: String,
    pub password: String,
}

/// Log in user
///
/// Encrypt SlimeUser data via IdentityService
pub fn login(
    identity: Identity,
    state: web::Data<State>,
    auth: web::Json<AuthData>,
) -> Result<(), Error> {
    let db = state.database.read().unwrap();
    let User {
        id,
        firstname,
        lastname,
        accounts,
        ..
    } = db.get_user(&auth.login, &auth.password)?.clone();
    let transaction_count = db.transaction_count(id);
    let slim = SlimUser {
        id,
        firstname,
        lastname,
        accounts,
        transaction_count,
    };

    // save json serialized user in identity
    let user_str = serde_json::to_string(&slim).map_err(Error::Json)?;
    identity.remember(user_str);

    Ok(())
}

/// Log out
pub fn logout(identity: Identity) {
    identity.forget();
}

impl FromRequest for SlimUser {
    type Error = ActixError;
    type Future = Result<SlimUser, ActixError>;
    type Config = ();

    fn from_request(req: &HttpRequest, p: &mut Payload) -> Self::Future {
        let id = Identity::from_request(req, p)?
            .identity()
            .ok_or(ErrorUnauthorized("token not authorized"))?;
        serde_json::from_str(&id).map_err(|_| ErrorUnauthorized("token not authorized"))
    }
}

/// get user
///
/// user is actually parsed from identity
pub fn get(user: SlimUser) -> web::Json<SlimUser> {
    web::Json(user)
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{dev::Service, test, App};

    fn initialized_state() -> State {
        let state = State::from_env();
        {
            let mut db = state.database.write().unwrap();
            db.register_user(
                "Login".to_string(),
                "Johann".to_string(),
                "Tuffe".to_string(),
                "My Password".to_string(),
            )
            .unwrap();
        }
        state
    }

    #[test]
    fn register_user() {
        let mut app = test::init_service(
            App::new()
                .data(State::from_env())
                .route("/", web::post().to(register)),
        );

        let user = RegisterUser {
            login: "Login".to_string(),
            firstname: "Johann".to_string(),
            lastname: "Tuffe".to_string(),
            password: "My Password".to_string(),
        };

        // registering a new user is ok
        let req = test::TestRequest::post()
            .uri("/")
            .set_json(&user)
            .to_request();
        let resp = test::block_on(app.call(req)).unwrap();
        assert!(resp.status().is_success(), "{:?}", resp);

        // registering the same user is a client error
        let req = test::TestRequest::post()
            .uri("/")
            .set_json(&user)
            .to_request();
        let resp = test::block_on(app.call(req)).unwrap();
        assert!(resp.status().is_client_error(), "{:?}", resp);
    }

    #[test]
    fn login_user() {
        let mut app = test::init_service(
            App::new()
                .data(initialized_state())
                .wrap(crate::identity_service())
                .route("/user", web::post().to(login))
                .route("/user", web::delete().to(logout))
                .route("/user", web::get().to(get)),
        );

        // login with correct user/password is ok
        let auth = AuthData {
            login: "Login".to_string(),
            password: "My Password".to_string(),
        };
        let req = test::TestRequest::post()
            .uri("/user")
            .set_json(&auth)
            .to_request();
        let resp = test::block_on(app.call(req)).unwrap();
        assert!(resp.status().is_success(), "{:?}", resp);

        // getting corresponding cookie
        let cookie = resp
            .response()
            .cookies()
            .find(|c| c.name() == "token")
            .expect("Cannot find 'token' cookie");

        // we can now get the user
        let req = test::TestRequest::get()
            .uri("/user")
            .cookie(cookie.clone())
            .to_request();
        let user: SlimUser = test::read_response_json(&mut app, req);
        assert_eq!(user.firstname, "Johann");

        // logout
        let req = test::TestRequest::delete()
            .uri("/user")
            .cookie(cookie)
            .to_request();
        let resp = test::block_on(app.call(req)).unwrap();
        assert!(resp.status().is_success(), "{:?}", resp);
        assert!(
            resp.response()
                .cookies()
                .all(|c| c.name() != "token" || c.value().is_empty()),
            "Found token cookie"
        );
    }

    #[test]
    fn login_user_wrong_password() {
        let mut app = test::init_service(
            App::new()
                .data(initialized_state())
                .wrap(crate::identity_service())
                .route("/user", web::post().to(login)),
        );

        // login with wrong user/password is a client error
        let auth = AuthData {
            login: "Login".to_string(),
            password: "My Wrong Password".to_string(),
        };
        let req = test::TestRequest::post()
            .uri("/user")
            .set_json(&auth)
            .to_request();
        let resp = test::block_on(app.call(req)).unwrap();
        assert!(resp.status().is_client_error(), "{:?}", resp);
    }

    #[test]
    fn login_user_invalid_login() {
        let mut app = test::init_service(
            App::new()
                .data(initialized_state())
                .wrap(crate::identity_service())
                .route("/user", web::post().to(login)),
        );

        // login with wrong user/password is a client error
        let auth = AuthData {
            login: "New Login".to_string(),
            password: "My Password".to_string(),
        };
        let req = test::TestRequest::post()
            .uri("/user")
            .set_json(&auth)
            .to_request();
        let resp = test::block_on(app.call(req)).unwrap();
        assert!(resp.status().is_client_error(), "{:?}", resp);
    }
}
