use crate::{errors::Error, users::SlimUser, State};
use actix_identity::Identity;
use actix_web::web;
use bank::{database::AccountId, transaction::Transaction};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct SlimTransaction {
    from: AccountId,
    to: AccountId,
    amount: f64,
}

/// Send a transaction
pub fn transfer(
    mut user: SlimUser,
    transaction: web::Json<SlimTransaction>,
    identity: Identity,
    state: web::Data<State>,
) -> Result<(), Error> {
    let db_count = {
        let db = state.database.read().unwrap();
        db.transaction_count(user.id)
    };
    if user.transaction_count == db_count {
        // current token is in sync with database transaction count, we can safely process

        let mut db = state.database.write().unwrap();
        db.transfer(transaction.from, transaction.to, transaction.amount)?;

        // update identity
        user.transaction_count += 1;
        let user_str = serde_json::to_string(&user).map_err(Error::Json)?;
        identity.remember(user_str);

        Ok(())
    } else {
        Err(Error::NotInSync)
    }
}

/// Get all transactions
pub fn transactions(
    user: SlimUser,
    account: web::Path<AccountId>,
    state: web::Data<State>,
) -> Result<web::Json<Vec<Transaction>>, Error> {
    if !user.accounts.contains(&account) {
        return Err(Error::InaccessibleAccount);
    }
    let db = state.database.read().unwrap();
    Ok(web::Json(db.transactions(account.into_inner())))
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{dev::Service, test, App};
    use bank::account::{Account, InternalAccount};

    fn initialized_state() -> State {
        let state = State::from_env();
        {
            let mut db = state.database.write().unwrap();
            db.register_user(
                "Login".to_string(),
                "Johann".to_string(),
                "Tuffe".to_string(),
                "My Password".to_string(),
            )
            .unwrap();
            // register 2 accounts to transfer between
            let mut account = InternalAccount::new(0);
            account.balance = 1000.;
            db.register_account(0, Account::Internal(account)).unwrap();
            db.register_account(0, Account::Internal(InternalAccount::new(0)))
                .unwrap();
        }
        state
    }

    #[test]
    fn transfer() {
        let mut app = test::init_service(
            App::new()
                .data(initialized_state())
                .wrap(crate::identity_service())
                .route("/user", web::post().to(crate::users::login))
                .route("/transfer", web::post().to(crate::transfer::transfer))
                .route(
                    "/transactions/{account}",
                    web::get().to(crate::transfer::transactions),
                ),
        );

        // login with correct user/password is ok
        let auth = crate::users::AuthData {
            login: "Login".to_string(),
            password: "My Password".to_string(),
        };
        let req = test::TestRequest::post()
            .uri("/user")
            .set_json(&auth)
            .to_request();
        let resp = test::block_on(app.call(req)).unwrap();
        assert!(resp.status().is_success(), "{:?}", resp);

        // getting corresponding cookie
        let cookie = resp
            .response()
            .cookies()
            .find(|c| c.name() == "token")
            .expect("Cannot find 'token' cookie");

        // transfer data
        let transaction = SlimTransaction {
            from: 0,
            to: 1,
            amount: 100.,
        };
        let req = test::TestRequest::post()
            .uri("/transfer")
            .cookie(cookie)
            .set_json(&transaction)
            .to_request();
        let resp = test::block_on(app.call(req)).unwrap();
        assert!(resp.status().is_success(), "{:?}", resp);

        let cookie = resp
            .response()
            .cookies()
            .find(|c| c.name() == "token")
            .expect("Cannot find 'token' cookie");

        // check that user has been updated
        let req = test::TestRequest::get()
            .uri("/transactions/0")
            .cookie(cookie)
            .to_request();
        let transactions: Vec<Transaction> = test::read_response_json(&mut app, req);
        assert_eq!(transactions[0].amount, -100.);
    }

    #[test]
    fn low_balance() {
        let mut app = test::init_service(
            App::new()
                .data(initialized_state())
                .wrap(crate::identity_service())
                .route("/user", web::post().to(crate::users::login))
                .route("/transfer", web::post().to(crate::transfer::transfer))
                .route(
                    "/transactions/{account}",
                    web::get().to(crate::transfer::transactions),
                ),
        );

        // login with correct user/password is ok
        let auth = crate::users::AuthData {
            login: "Login".to_string(),
            password: "My Password".to_string(),
        };
        let req = test::TestRequest::post()
            .uri("/user")
            .set_json(&auth)
            .to_request();
        let resp = test::block_on(app.call(req)).unwrap();
        assert!(resp.status().is_success(), "{:?}", resp);

        // getting corresponding cookie
        let cookie = resp
            .response()
            .cookies()
            .find(|c| c.name() == "token")
            .expect("Cannot find 'token' cookie");

        // transfer data
        let transaction = SlimTransaction {
            from: 1,
            to: 0,
            amount: 100.,
        };
        let req = test::TestRequest::post()
            .uri("/transfer")
            .cookie(cookie)
            .set_json(&transaction)
            .to_request();
        let resp = test::block_on(app.call(req)).unwrap();
        assert!(resp.status().is_client_error(), "{:?}", resp);
    }

    #[test]
    fn repeat_transfer() {
        let mut app = test::init_service(
            App::new()
                .data(initialized_state())
                .wrap(crate::identity_service())
                .route("/user", web::post().to(crate::users::login))
                .route("/transfer", web::post().to(crate::transfer::transfer))
                .route(
                    "/transactions/{account}",
                    web::get().to(crate::transfer::transactions),
                ),
        );

        // login with correct user/password is ok
        let auth = crate::users::AuthData {
            login: "Login".to_string(),
            password: "My Password".to_string(),
        };
        let req = test::TestRequest::post()
            .uri("/user")
            .set_json(&auth)
            .to_request();
        let resp = test::block_on(app.call(req)).unwrap();
        assert!(resp.status().is_success(), "{:?}", resp);

        // getting corresponding cookie
        let cookie = resp
            .response()
            .cookies()
            .find(|c| c.name() == "token")
            .expect("Cannot find 'token' cookie");

        // transfer data
        let transaction = SlimTransaction {
            from: 0,
            to: 1,
            amount: 100.,
        };
        let req = test::TestRequest::post()
            .uri("/transfer")
            .cookie(cookie.clone())
            .set_json(&transaction)
            .to_request();
        let resp = test::block_on(app.call(req)).unwrap();
        assert!(resp.status().is_success(), "{:?}", resp);

        // try repeating the same transfer, without updating the cookie
        // simulates when user didn't have a response
        let req = test::TestRequest::post()
            .uri("/transfer")
            .cookie(cookie)
            .set_json(&transaction)
            .to_request();
        let resp = test::block_on(app.call(req)).unwrap();
        assert!(resp.status().is_client_error(), "{:?}", resp);
    }
}
