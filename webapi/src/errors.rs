//! A module to handle all webservice errors

use actix_web::{error::ResponseError, HttpResponse};
use bank::errors::Error as BankError;
use std::fmt;

/// Webservive Error
#[derive(Debug)]
pub enum Error {
    /// Error from Bank
    Bank(BankError),
    /// Error when (de)serializing json
    Json(serde_json::Error),
    /// Synchronization mismatch
    NotInSync,
    /// Account not accessible for this user
    InaccessibleAccount,
}

impl From<bank::errors::Error> for Error {
    fn from(e: BankError) -> Self {
        Error::Bank(e)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            Error::Bank(e) => write!(f, "Bank Error: {:?}", e),
            Error::Json(e) => write!(f, "Json serialization error: {:?}", e),
            Error::NotInSync => write!(f, "Current token not in sync with database, please relog"),
            Error::InaccessibleAccount => write!(f, "Account not accessible"),
        }
    }
}

impl ResponseError for Error {
    fn error_response(&self) -> HttpResponse {
        log::error!("{}", self);
        match self {
            Error::Bank(BankError::DuplicateLogin(l)) => {
                HttpResponse::Conflict().json(&format!("Duplicate Login '{}'", l))
            }
            Error::Bank(BankError::LoginNotFound(l)) => {
                HttpResponse::NotFound().json(&format!("Login '{}' not found", l))
            }
            Error::Bank(BankError::InvalidPassword(l)) => {
                HttpResponse::Forbidden().json(&format!("Duplicate Login '{}'", l))
            }
            Error::Bank(BankError::InvalidUserId(_)) | Error::Bank(BankError::Argon(_)) => {
                HttpResponse::InternalServerError().finish()
            }
            Error::Bank(BankError::LowBalance) => {
                HttpResponse::Forbidden().json(&format!("Balance is not sufficient to proceed"))
            }
            Error::Json(e) => HttpResponse::InternalServerError().json(e.to_string()),
            Error::NotInSync => {
                HttpResponse::Forbidden().json("Cannot process, please logout and login again")
            }
            Error::InaccessibleAccount => HttpResponse::Forbidden()
                .json("Cannot process, account not accessible for this user"),
        }
    }
}
