mod accounts;
mod errors;
mod transfer;
mod users;

use actix_identity::{CookieIdentityPolicy, IdentityService};
use actix_web::{middleware, web, App, HttpServer};
use bank::database::Database;
use std::env;
use std::sync::RwLock;

pub const SALT: &str = "BANK_SALT";
pub const SECRET: &str = "IDENTITY_SECRET";
pub const DOMAIN: &str = "DOMAIN";

/// Application state
pub struct State {
    /// Database
    pub database: RwLock<Database>, // In a real world this is a pool to a real database
}

impl State {
    fn new(salt: String) -> Self {
        let database = RwLock::new(Database::new(salt));
        State { database }
    }

    pub(crate) fn from_env() -> Self {
        let salt = env::var(SALT).unwrap_or_else(|_| "My Bank Salt".to_string());
        State::new(salt)
    }
}

/// Get IdentityService from env variable names
pub(crate) fn identity_service() -> IdentityService<CookieIdentityPolicy> {
    let secret = env::var(SECRET)
        .unwrap_or_else(|_| "My Identity Secret must be at least 32 bytes long".to_string());
    let domain = env::var(DOMAIN).unwrap_or_else(|_| "localhost".to_string());

    IdentityService::new(
        CookieIdentityPolicy::new(secret.as_bytes())
            .domain(&domain)
            .name("token")
            .path("/")
            .max_age_time(chrono::Duration::seconds(60 * 10))
            .secure(false),
    )
}

fn main() {
    env::set_var("RUST_LOG", "actix_web=info,actix_server=info");
    env_logger::init();

    let state = web::Data::new(State::from_env());

    HttpServer::new(move || {
        App::new()
            .register_data(state.clone())
            .wrap(middleware::Logger::default())
            .wrap(identity_service())
            .service(
                web::scope("/register")
                    .route("/user", web::post().to(crate::users::register))
                    .route("/account", web::post().to(crate::accounts::register)),
            )
            .service(
                web::resource("/user")
                    .route(web::post().to(crate::users::login))
                    .route(web::delete().to(crate::users::logout))
                    .route(web::get().to(crate::users::get)),
            )
            .service(web::resource("/transfer").route(web::post().to(crate::transfer::transfer)))
            .service(
                web::resource("/transactions/{account}")
                    .route(web::get().to(crate::transfer::transactions)),
            )
    })
    .bind("127.0.0.1:3000")
    .unwrap()
    .run()
    .unwrap();
}
