use crate::{errors::Error, users::SlimUser, State};
use actix_identity::Identity;
use actix_web::web;
use bank::account::Account;

/// Register a new account
pub fn register(
    identity: Identity,
    mut user: SlimUser,
    account: web::Json<Account>,
    state: web::Data<State>,
) -> Result<(), Error> {
    let mut db = state.database.write().unwrap();

    let id = db.register_account(user.id, account.into_inner())?;
    user.accounts.push(id);

    // save json serialized user in identity
    let user_str = serde_json::to_string(&user).map_err(Error::Json)?;
    identity.remember(user_str);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{dev::Service, test, App};
    use bank::account::{Account, InternalAccount};

    fn initialized_state() -> State {
        let state = State::from_env();
        {
            let mut db = state.database.write().unwrap();
            db.register_user(
                "Login".to_string(),
                "Johann".to_string(),
                "Tuffe".to_string(),
                "My Password".to_string(),
            )
            .unwrap();
        }
        state
    }

    #[test]
    fn register_account() {
        let mut app = test::init_service(
            App::new()
                .data(initialized_state())
                .wrap(crate::identity_service())
                .route("/user", web::post().to(crate::users::login))
                .route(
                    "/register/account",
                    web::post().to(crate::accounts::register),
                ),
        );

        // login with correct user/password is ok
        let auth = crate::users::AuthData {
            login: "Login".to_string(),
            password: "My Password".to_string(),
        };
        let req = test::TestRequest::post()
            .uri("/user")
            .set_json(&auth)
            .to_request();
        let resp = test::block_on(app.call(req)).unwrap();
        assert!(resp.status().is_success(), "{:?}", resp);

        // getting corresponding cookie
        let cookie = resp
            .response()
            .cookies()
            .find(|c| c.name() == "token")
            .expect("Cannot find 'token' cookie");

        // register a new account on user 0
        let account = Account::Internal(InternalAccount::new(0));
        let req = test::TestRequest::post()
            .uri("/register/account")
            .cookie(cookie.clone())
            .set_json(&account)
            .to_request();
        let resp = test::block_on(app.call(req)).unwrap();
        assert!(resp.status().is_success(), "{:?}", resp);

        // cookie has changed because of the newly added account
        let new_cookie = resp
            .response()
            .cookies()
            .find(|c| c.name() == "token")
            .expect("Cannot find 'token' cookie");
        assert!(cookie != new_cookie, "Cookie must have changed");
    }
}
