# bank.one

This repository implements a very basic bank api.

It is developed using Rust stable with actix-web for web framework.

## Installation

1. [Install rust][rust install]
2. Clone the repository
```sh
git clone https://gitlab.com/tafia/bank.one
cd bank.one
```
3. Run tests
Use cargo (installed with rust) to build, test and generate corresponding documentation.

```sh
cd webapi
cargo test # for tests
cargo run --release # to run with all optimizations enabled
cd ../bank
cargo doc -p bank --open # to build and open bank crate documentation
```

## Projects

### bank

This crate corresponds to the bank logic. 

In a real world it should work with an underlying database.
Here all tables are represented with simple `Vec`.

### webapi

Webservice implementation.

[swagger][swagger]

The webservice is implemented using actix-web. As of the time of writting the binary,
async-await is not stable in rust, so this project doesn't benefit from it.

Also as we do not have a real database implementation, I/O is blocking (using a `Mutex`).
In a real world, we would use a pool to database connection to handle many requests in parallel.

#### Authentication

Once a user has been registered, he needs to log-in to the service to get access to his data.
The authentication process uses expirable cookies to communicate with user. Data is encrypted, expires
and whenever the user account has any change, the corresponding cookie value changes as well.

#### Environment variables

- `BANK_SALT`: This variable is used as the salt when encrypting user password using the Argon2 algorithm
- `IDENTITY_SECRET`: This variable is used to hash the current user data once logged-in.
- `DOMAIN`: The domain used for identity cookie


[rust install]: https://www.rust-lang.org/tools/install
[swagger]: webapi/swagger.json
