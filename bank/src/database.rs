//! A module to handle persistence

use crate::account::Account;
use crate::errors::Error;
use crate::transaction::Transaction;
use crate::user::User;
use std::collections::{hash_map::Entry, HashMap};

/// A User id
pub type UserId = usize;
/// An Account Id
pub type AccountId = usize;
/// A Transaction Id
pub type TransactionId = usize;

/// A Login type
pub type Login = String;

/// A Database implementation
///
/// In real life this would likely be implemented using a database
#[derive(Default, Debug)]
pub struct Database {
    salt: String,
    logins: HashMap<Login, UserId>,
    users: Vec<User>,
    accounts: Vec<Account>,
    transactions: Vec<Transaction>,
}

impl Database {
    /// Creates a new database with given salt used for password hashing
    pub fn new(salt: String) -> Self {
        Database {
            salt,
            ..Database::default()
        }
    }

    /// Registers a new user
    pub fn register_user(
        &mut self,
        login: Login,
        firstname: String,
        lastname: String,
        password: String,
    ) -> Result<UserId, Error> {
        match self.logins.entry(login) {
            Entry::Occupied(e) => Err(Error::DuplicateLogin(e.key().to_string())),
            Entry::Vacant(e) => {
                // hash the real password so it won't be saved in the database
                let config = argon2::Config::default();
                let password_hash =
                    argon2::hash_encoded(password.as_bytes(), self.salt.as_bytes(), &config)?;

                let id = self.users.len();
                e.insert(id);
                self.users
                    .push(User::new(id, firstname, lastname, password_hash));
                Ok(id)
            }
        }
    }

    /// Try getting a user from the database
    pub fn get_user(&self, login: &str, password: &str) -> Result<&User, Error> {
        let user_id = *self
            .logins
            .get(login)
            .ok_or_else(|| Error::LoginNotFound(login.to_string()))?;

        let user = &self.users[user_id];

        if argon2::verify_encoded(&user.password_hash, password.as_bytes())? {
            Ok(user)
        } else {
            Err(Error::InvalidPassword(login.to_string()))
        }
    }

    /// Registers a new account
    pub fn register_account(
        &mut self,
        user_id: UserId,
        account: Account,
    ) -> Result<AccountId, Error> {
        let user = self
            .users
            .get_mut(user_id)
            .ok_or(Error::InvalidUserId(user_id))?;
        let account_id = self.accounts.len();
        self.accounts.push(account);
        user.accounts.push(account_id);
        Ok(account_id)
    }

    /// Get the total number of transactions for this user
    pub fn transaction_count(&self, user: UserId) -> usize {
        self.users[user]
            .accounts
            .iter()
            .filter_map(|a| self.accounts.get(*a))
            .filter_map(|a| a.transaction_count())
            .sum()
    }

    /// Get all transactions for this account
    pub fn transactions(&self, account: AccountId) -> Vec<Transaction> {
        if let Some(transactions) = self.accounts[account].transactions() {
            transactions
                .iter()
                .map(|&t| self.transactions[t].clone())
                .collect()
        } else {
            Vec::new()
        }
    }

    /// Make a new transfer
    pub fn transfer(&mut self, from: AccountId, to: AccountId, amount: f64) -> Result<(), Error> {
        // create new transactions, without committing then yet
        let from_transaction = Transaction::new(from, to, -amount);
        let to_transaction = Transaction::new(to, from, amount);

        // check if we can send this transaction
        self.accounts[from].check(&from_transaction)?;
        self.accounts[to].check(&to_transaction)?;

        // commit transactions in db
        let from_id = self.transactions.len();
        self.transactions.push(from_transaction);
        let to_id = self.transactions.len();
        self.transactions.push(to_transaction);

        // notify accounts that transactions have been sent
        let res = self.accounts[from]
            .push(-amount, from_id)
            .and_then(|_| self.accounts[to].push(amount, to_id));

        // if we couldn't notify, rollback the transactions
        if res.is_err() {
            self.transactions.truncate(from_id);
        }

        res
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn register_user() -> Result<(), Error> {
        let mut db = Database::new("my argon salt".to_string());
        let login = "user";
        let password = "password";
        db.register_user(login.into(), String::new(), String::new(), password.into())?;
        let _id = db.get_user(login, password)?;
        Ok(())
    }

    #[test]
    #[should_panic(expected = "InvalidPassword")]
    fn wrong_password() {
        let mut db = Database::new("my argon salt".to_string());
        let login = "user";
        let password = "password";
        db.register_user(login.into(), String::new(), String::new(), password.into())
            .unwrap();
        db.get_user(login, "wrong password").unwrap();
    }

    #[test]
    #[should_panic(expected = "LoginNotFound")]
    fn login_not_found() {
        let mut db = Database::new("my argon salt".to_string());
        let login = "user";
        let password = "password";
        db.register_user(login.into(), String::new(), String::new(), password.into())
            .unwrap();
        db.get_user("new user", password).unwrap();
    }

    #[test]
    #[should_panic(expected = "DuplicateLogin")]
    fn duplicate_login() {
        let mut db = Database::new("my argon salt".to_string());
        let login = "user";
        let password = "password";
        db.register_user(login.into(), String::new(), String::new(), password.into())
            .unwrap();
        db.register_user(
            login.into(),
            String::new(),
            String::new(),
            "new passowrd".into(),
        )
        .unwrap();
    }
}
