//! A module to handle Accounts

use crate::database::{TransactionId, UserId};
use crate::errors::Error;
use crate::transaction::Transaction;
use serde::{Deserialize, Serialize};

/// An account
#[derive(Debug, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum Account {
    /// A internal account
    Internal(InternalAccount),
    /// An external account, example with HSBC
    HSBC(HSBCAccount),
}

impl Account {
    /// Check if the account can accept the transaction
    pub fn check(&self, transaction: &Transaction) -> Result<(), Error> {
        match self {
            Account::Internal(a) => a.check(transaction),
            Account::HSBC(a) => a.check(transaction),
        }
    }

    /// Push transaction to account
    pub fn push(&mut self, amount: f64, id: TransactionId) -> Result<(), Error> {
        match self {
            Account::Internal(a) => a.push(amount, id),
            Account::HSBC(a) => a.push(amount, id),
        }
    }

    /// Get correponding user id, if it is an internal account
    pub fn user(&self) -> Option<UserId> {
        match self {
            Account::Internal(a) => Some(a.user),
            _ => None,
        }
    }

    /// Number of transaction for this account
    pub fn transaction_count(&self) -> Option<usize> {
        match self {
            Account::Internal(a) => Some(a.transactions.len()),
            _ => None,
        }
    }

    /// Number of transaction for this account
    pub fn transactions(&self) -> Option<&[usize]> {
        match self {
            Account::Internal(a) => Some(&a.transactions),
            _ => None,
        }
    }
}

/// Internal account
#[derive(Debug, Default, Serialize, Deserialize)]
pub struct InternalAccount {
    /// Owner id
    pub user: UserId,
    /// Current balance
    pub balance: f64,
    /// All transactions on this account
    pub transactions: Vec<TransactionId>,
}

impl InternalAccount {
    /// Create a new internal account
    pub fn new(user: UserId) -> Self {
        InternalAccount {
            user,
            ..InternalAccount::default()
        }
    }

    /// Check if the account can accept the transaction
    fn check(&self, transaction: &Transaction) -> Result<(), Error> {
        if self.balance + transaction.amount > 0. {
            Ok(())
        } else {
            Err(Error::LowBalance)
        }
    }

    /// Push transaction to account
    fn push(&mut self, amount: f64, id: TransactionId) -> Result<(), Error> {
        self.balance += amount;
        self.transactions.push(id);
        Ok(())
    }
}

/// A HSBC account
///
/// Not implemented
#[derive(Debug, Serialize, Deserialize)]
pub struct HSBCAccount {}

impl HSBCAccount {
    /// Check if the account can accept the transaction
    fn check(&self, _transaction: &Transaction) -> Result<(), Error> {
        // Shoud use HSBC API here
        Ok(())
    }

    /// Push transaction to account
    fn push(&mut self, _amount: f64, _id: TransactionId) -> Result<(), Error> {
        // Should use HSBC API here
        Ok(())
    }
}
