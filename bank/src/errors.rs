//! A module to handle errors

use crate::database::{Login, UserId};

/// A bank error
#[derive(Debug)]
pub enum Error {
    /// Login already registered
    DuplicateLogin(Login),
    /// Login not found
    LoginNotFound(Login),
    /// Invalid login/password
    InvalidPassword(Login),
    /// Invalid User Id
    InvalidUserId(UserId),
    /// Error while hashing using Argon2
    Argon(argon2::Error),
    /// Low balance
    LowBalance,
}

impl From<argon2::Error> for Error {
    fn from(e: argon2::Error) -> Self {
        Error::Argon(e)
    }
}
