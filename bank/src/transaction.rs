//! A module to handle transactions

use crate::database::AccountId;
use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};

/// A Transaction
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Transaction {
    /// Account from which the amount has been debited
    pub from_account: AccountId,
    /// Account to which the amount has been credited
    pub to_account: AccountId,
    /// Timestamp when the order has been received
    pub timestamp: NaiveDateTime,
    /// Amount transferred
    pub amount: f64,
}

impl Transaction {
    /// Creates a new `Transaction`
    pub fn new(from_account: AccountId, to_account: AccountId, amount: f64) -> Self {
        let timestamp = chrono::offset::Local::now().naive_local();
        Transaction {
            from_account,
            to_account,
            timestamp,
            amount,
        }
    }
}
