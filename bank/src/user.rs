//! A module to handle Users

use crate::database::{AccountId, UserId};
use serde::{Deserialize, Serialize};

/// A User Name
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct User {
    /// Unique user id, assigned by database
    pub id: UserId,
    /// User first name
    pub firstname: String,
    /// User last name
    pub lastname: String,
    /// A password (hashed)
    pub(crate) password_hash: String,
    /// User accounts (either own internal or external)
    pub accounts: Vec<AccountId>,
}

impl User {
    /// Creates a new `User`
    pub fn new(id: UserId, firstname: String, lastname: String, password_hash: String) -> Self {
        User {
            id,
            firstname,
            lastname,
            password_hash,
            accounts: Vec::new(),
        }
    }
}
