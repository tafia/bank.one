//! # bank
//!
//! A crate to handle banking operations

#![deny(missing_docs)]

pub mod account;
pub mod database;
pub mod errors;
pub mod transaction;
pub mod user;
